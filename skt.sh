#!/usr/bin/env bash

# Files to use with skt
files="file1 file2" # add a new file like this: "file1 file2 file3"



# Pull the latest changes from the repo
# git pull

skt() {
	git pull
	# #################
	# Upload Sketch files to git
	# #################
	if [ "$1" == "up" ]; then
		if [ -z "$2" ]; then
			echo "This command involves commiting and uploading to git. You need to enter a commit message after with the 'up' command."
			return
		fi
		
		# Keep preview image option
		if [[ "$3" =~ -[Pp]$ ]] || [[ "$4" =~ -[Pp]$ ]]; then
			export keepPreviews=true
		fi
		
		# Delete .sketch file
		if [[ "$3" =~ -[Dd]$ ]] || [[ "$4" =~ -[Dd]$ ]]; then
			export forceDelete=true
		fi
		
		
		echo "Uploading $f.sketch..."
		
		for f in $files
		do
			if ! [ -e "$f" ]; then
				echo "$f.sketch doesn't exist or the path to it is incorrect."
				return
			fi
			
			# Copy .sketch to .zip
			cp "$f".sketch "$f".zip

			# Unzip the file and delete
			unzip -o "$f".zip -d "$f"/
			rm -Rf "$f".zip
			
			# Optionally Remove the preview file
			if ! [ $keepPreviews ]; then
				rm -Rf "$f"/previews/
				echo "Previews removed!" #TODO: Remove
			fi
			
			# Optionally remove sketch files
			if [ $forceDelete ]; then
				echo ""
				echo "Removing $f.sketch..."
				rm -Rf "$f".sketch
			else
				read -p "Do you want to remove $f.sketch? (y/n)  " -n 1 -r;
				if [[ $REPLY =~ ^[Yy]$ ]]; then
					echo ""
					echo "Removing $f.sketch..."
					rm -Rf "$f".sketch
				else
					echo ""
					echo "Leaving $f.sketch in place"
				fi #if y/n
			fi #if $forceDelete
		
			git add "$f"/
		done
		
		# commit and push
		git commit -m "$2" && git push

		unset keepPreviews forceDelete
		echo "🎉 Success! 🎉"
		return

	# #################
	# Download Sketch files from git
	# #################
	elif [ "$1" == "down" ]; then
		echo "Downloading your sketch files..."
		
		for f in $files
		do
			if ! [ -d "$f" ]; then
				echo "The directory ($f) doesn't exist or the path to it is incorrect."
				return
			fi
			# Zip core Sketch data
			cd "$f"/ && zip "$f".zip -r .
			
			# Copy .zip to .sketch
			cp "$f".zip ../"$f".sketch
			
			# Remove zip
			rm -Rf "$f".zip && cd ..
			
			# Optionally remove sketch-code files
			if [[ "$2" =~ -[Dd]$ ]]; then
				echo "Removing $f..."
				rm -Rf "$f"
			else
				read -p "Do you want to remove $f (code files for $f.sketch)? (y/n)  " -n 1 -r;
				if [[ $REPLY =~ ^[Yy]$ ]]; then
					echo ""
					echo "Removing $f..."
					rm -Rf "$f"
				else
					echo ""
					echo "Leaving $f in place"
				fi #if Y/N
			fi #if -D

		done
		echo "🎉 Success! 🎉"
		return
	
	# #################
	# Provide error feedback
	# #################
	else
		echo "skt is usefull for uploading and downloading files in Git."
		echo "To upload (with options):"
		echo "skt up <git commit message> <-D> (auto delete .sketch files) <-P> (Keeps preview images)"
		echo "skt down <-D> (auto delete .sketch core files)"
		return
	fi
}